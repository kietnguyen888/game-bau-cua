import React, { Component } from 'react'

import ContextProvider from './Hooks/Context/ContextProvider'

import BaiTapGameBauCua from './BaiTapTongHop/BaiTapGameBauCua/BaiTapGameBauCua'

export default class App extends Component {


  render() {
    return (
      <ContextProvider>
        
          <BaiTapGameBauCua />
      </ContextProvider>
    )
  }
}
